let app = angular.module("steemcraft.com", ["ngRoute", "ngSanitize"]);

app.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');
}]);

//todo: wydzielenie controllerow do oddzielnych plikow
//todo: pageinacja strony glownej
//todo: szukajka?
//todo: nesting(?) komentarzy
//todo: moze jakas nagroda za codzienny vote?
//todo: fallback nodes
//todo: loadingi?

app.run(function ($rootScope) {
    $rootScope.wif = localStorage['user.wif'];
    $rootScope.title = "Strona glowna"; //todo: nie dziala?
    $rootScope.userName = localStorage['user.username'];

    //todo: ogarnac to jakos szybciej, chociaz jakies podstawowe rzeczy co by user nie musial czekac np. na pojawienie sie jego nicku w nawigacji
    steem.api.getAccounts([localStorage['user.username']], function (err, result) { //mozna to manualnie odswiezyc - $rootScope.refreshUserInformation();
        $rootScope.user = result[0];
        $rootScope.$applyAsync();
    });
    console.log($rootScope.wif);
    console.log($rootScope.user);

    $rootScope.isAuth = function () {
        return localStorage['user.loggedIn'];
    };

    $rootScope.logout = function () {
        delete $rootScope.wif;
        delete $rootScope.user;
        delete $rootScope.userName;
        localStorage.removeItem("user.wif");
        localStorage.removeItem("user.username");
        localStorage.removeItem("user.loggedIn");
        $rootScope.$applyAsync();
    };

    $rootScope.refreshUserInformation = function () {
        steem.api.getAccounts([localStorage['user.username']], function (err, result) {
            $rootScope.user = result[0];
            $rootScope.$applyAsync();
        });
    };

    $rootScope.$applyAsync();
});

app.controller('Main', function ($scope) {
    $scope.loadPosts = function () {
        var posts = [];
        steem.api.getBlogEntries("steemcraft.com", 9999, 15, function (err, data) {
            for (var i = 0; i < data.length; i++) {
                steem.api.getContent(data[i].author, data[i].permlink, function (err, result) {
                    var body = result.body;
                    if (body.length >= 500) {
                        body = body.substring(0, 499) + "... **[(Click to read full text.)](blog/" + result.permlink + ")**";
                    }
                    posts.push({
                        'author': result.author,
                        'title': result.title,
                        'body': marked(body),
                        'date': moment(Date.parse(result.created)).format('DD.MM.YYYY HH:mm'),
                        'dateInt': Date.parse(result.created),
                        'permlink': result.permlink
                    });
                    if (data.length === i) {
                        posts.sort(function (a, b) {
                            return b.dateInt - a.dateInt
                        });
                        console.log(posts);
                        $scope.posts = posts;
                        $scope.$applyAsync();
                    }
                });
            }
        });
    };
});

app.controller("PostController", function ($scope, $rootScope, $routeParams) {
    $scope.loadPost = function () {
        steem.api.getContent("steemcraft.com", $routeParams.permlink, function (err, result) {
            console.log(err, result);
            $scope.post = {
                'author': result.author,
                'title': result.title,
                'body': marked(result.body),
                'date': moment(Date.parse(result.created)).format('DD.MM.YYYY HH:mm'),
                'dateInt': Date.parse(result.created),
                'permlink': result.permlink,
                'repliesNumber': result.replies.length
            };
            $scope.$applyAsync();
        });
    };

    $scope.loadComments = function () {
        steem.api.getContentReplies("steemcraft.com", $routeParams.permlink, function (err, result) {
            if (!err) {
                console.log(result);
                //TODO: do not display bot comments :3
                $scope.comments = result.slice(-25).reverse();
                $scope.$applyAsync();
            }
        })
    };

    $scope.vote = function (author, permlink, weight) {
        steem.broadcast.vote($rootScope.wif, $rootScope.user.name, author, permlink, weight, function (err, result) {
            console.log(err, result);
            $scope.loadComments();
        });
    };

    $scope.votePost = function () {
        steem.broadcast.vote($rootScope.wif, $rootScope.user.name, "steemcraft.com", $routeParams.permlink, 10000, function (err, result) {
            console.log(err, result);
            $scope.loadPost();
        });
    };

    $scope.comment = function () {
        var permlink = steem.formatter.commentPermlink("steemcraft.com", $routeParams.permlink);
        $scope.loading = true;
        let operations = [
            ['comment',
                {
                    parent_author: "steemcraft.com",
                    parent_permlink: $routeParams.permlink,
                    author: $scope.user.name,
                    permlink: permlink,
                    title: 'steemcraft.com',
                    body: $scope.message,
                    json_metadata: JSON.stringify({
                        format: "html",
                        app: "steemcraft.com"
                    })
                }
            ],
            ['comment_options', {
                author: $scope.user.name,
                permlink: permlink,
                max_accepted_payout: '1000000.000 SBD',
                percent_steem_dollars: 10000,
                allow_votes: true,
                allow_curation_rewards: true,
                extensions: [
                    [0, {
                        beneficiaries: [
                            {account: 'steemcraft.com', weight: 1000},
                        ]
                    }]
                ]
            }]
        ];

        steem.broadcast.sendAsync({
            extensions: [],
            operations: operations
        }, {posting: localStorage['user.wif']}, (err, result) => {
            console.log(err, result);
            if (!err) {
                $scope.message = '';
                $scope.loading = false;
                $scope.loadComments();
            }
        });

    }
});

app.controller('LoginController', function ($scope, $rootScope, $location) {
    $scope.loginInProgress = false;

    //"working" code.
    $scope.login = function () {
        let login = $scope.loginNickname;
        let password = $scope.loginPassword;
        console.log(login + " " + password);
        steem.api.getAccounts([login], function (err, result) {
            if (result.length === 0) {
                console.log("Account " + login + " does not exist.");
                alert("Konto o nazwie " + login + " nie istnieje. Zaloz je za darmo na signup.steemit.com") //TODO: sensowniejsza informacja o nieistniejacym koncie
            } else { //acount exists
                let account = result[0];
                let account_posting_public_key = account['posting']['key_auths'][0][0];
                console.log("Posting public key for @" + login + " account: " + account_posting_public_key);
                let postingPrivateKey;

                function a(callback) {
                    if (steem.auth.isWif(password)) {
                        postingPrivateKey = password;
                    } else {
                        postingPrivateKey = steem.auth.toWif(login, password, "posting");
                    }
                    callback();
                }

                a(function () {
                    console.log("Posting private key for @" + login + " account: " + postingPrivateKey);
                    if (steem.auth.wifIsValid(postingPrivateKey, account_posting_public_key)) {
                        localStorage["user.wif"] = postingPrivateKey;
                        localStorage["user.username"] = account["name"];
                        localStorage['user.loggedIn'] = true;
                        console.log("correct!");
                        $rootScope.refreshUserInformation();
                        $scope.$applyAsync();
                        $location.path("/");
                    } else {
                        alert("Niepoprawne haslo."); //TODO: sensowniejsza informacja o niepoprawnym hasle.
                    }
                });
            }
            console.log(err, result);
        });

        $scope.$applyAsync();
        $rootScope.$applyAsync();
    }
});

app.controller('ProfileController', function ($scope) {

});

app.controller('LogoutController', function ($scope, $rootScope) {
    $rootScope.logout();
});

app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            controller: "Main",
            templateUrl: "app/views/index.html"
        })
        .when("/login", {
            controller: "LoginController",
            templateUrl: "app/views/login.html"
        })
        .when("/logout", {
            controller: "LogoutController",
            templateUrl: "app/views/logout.html"
        })
        .when("/profile", {
            controller: "ProfileController",
            templateUrl: "app/views/profile.html"
        })
        .when("/blog/:permlink", {
            controller: "PostController",
            templateUrl: "app/views/blogPost.html"
        })
        .otherwise({
            controller: "Main",
            templateUrl: 'app/views/errors/404.html' //TODO: create error pages.
        });
});